// alert("gm!!");

// array - single variable with multiple values (no specificity)
// object - contains multiple values (which are easily labeled)
	// -use properties instead of index
	// -values can have labels

//JavaScript Objects
	/*
	Syntax:
		let objectName = {
			key : value
			key : (function)
			key : object {
					key : value
			}
			key : [array]
		}
	*/

let cellphone = {
	name : "Nokia 3210",
	manufactureDate : 1999
};

console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a Constructor function
	/*
	Syntax:
		
		function objectName (keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB;
		}

	*/


	function Laptop(name, manufactureDate) {
		this.name = name;
		this.releaseDate = manufactureDate;
	};

	// new operator creates an object
	let laptop = new Laptop("Lenovo", 2008);
	console.log(laptop); 
		// will display new object
		//

	let laptop1 = Laptop("Asus", 2010); 
	console.log(laptop1);
		//result: undefined


// We can also create empty objects:
	let computer = {};
	let myComputer = new Object();
	console.log(myComputer);
	console.log(computer);


// Accessing Array Objects ----- use a dot notation (.name) or ""
					// dot notation is better. "" may be confusing

let machines = [laptop, laptop1];

	//access property of object inside an array 
	console.log(machines[0].name)
		// result: Lenovo
	console.log(machines[0]["releaseDate"]);


// Initializing, Adding, Deleting, Reassigning Object Properties

let car = {};

	// initializing/adding object properties using dot notation

	car.name = "Honda Civic";
	console.log(car);

	// adding object properties using [square brackets]

	car["manufactureDate"] = 2019;
	console.log(car);

	// deleting object properties

	delete car["name"];
	console.log(car);

	// mini Activity

	car.brand = "Toyota",
	car.model = "Altis",
	car.color = "white",
	car.gasType = "Diesel",
	car.location = "Baguio City"
	console.log(car);

	// reassigning value of object properties

	car.manufactureDate = 1986;
	console.log(car);


// Object Methods 
		// - function which is a property of an object
		// - function that is related to the object
		// - methods are defined based on what an object is capable of doing & how it should work

let person = {
	name: "Jin",
	talk: function(){
		console.log(`Hello my name is ${this.name}`)
	}
};

person.talk();

let friend = {
	firstName: "Bam",
	lastName: "Jeon",
	address: {
		city: "Caloocan",
		country: "Philippines"
	},
	emails: ["bam@mail.com", "jeonbam@gmail.com"],
	introduce: function() {
		console.log(`Hello, my name is ${this.firstName} ${this.lastName}, 
			I live in ${this.address.city}`)
	}
};
friend.introduce();

// real world application

let myPokemon = {
	name: "Bulbasaur",
	level: 3, 
	health: 100, 
	attack: 50,
	tackle: function() {
		console.log(`${this.name} tackled another pokemon`)
		console.log(`targetPokemon's health is now reduced`)
	},
	faint: function() {
		console.log(`${this.name} fainted`)
	}
};

myPokemon.tackle();
myPokemon.faint();

	// use object constructor 

	function Pokemon(name, lvl, hp){
		//Properties
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;

		//methods
		this.tackle = function(target){
			console.log(`${this.name} tackled ${target.name}`);
			console.log(`targetPokemon's health is now reduced`);
			console.log(target.name);
			console.log(this.name);
		};
		this.faint = function() {
			console.log(`${this.name} fainted`);
		}
	};

	// create new instances of the Pokemon object each w/ their unique properties

	let pikachu = new Pokemon("Pikachu", 3, 50);
	let ratata = new Pokemon("Ratata", 5, 25);

	// Providing the "ratata" object as an argument to "pikachu" tackle method
	// will cteate interaction between two objects

	pikachu.tackle(ratata);
	ratata.tackle(pikachu);

	