// alert("Pokemon"); 

function Pokemon(name, lvl, hp){
		//Properties
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;

		//methods
		this.tackle = function(target){
			console.log(`${this.name} tackled ${target.name}`);
			target.health = target.health - this.attack;
			console.log(`${target.name}'s health is now reduced to ${target.health}`);
			if (target.health < 10) {
				target.faint();
			}	
		};
		this.faint = function() {
			console.log(`${this.name} fainted`);
		};
/*		this.attack = function() {
		}*/
	};

	let torchic = new Pokemon ("Torchic", 5, 15); 
	let mew = new Pokemon ("Mew", 7, 35);

	mew.tackle(torchic);
	torchic.tackle(mew);
	mew.tackle(torchic);
	torchic.tackle(mew);
	mew.tackle(torchic);
	

